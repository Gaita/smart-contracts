// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.7.0 <0.9.0;



abstract contract Context {
    function _msgSender() internal view virtual returns (address) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes calldata) {
        this; // silence state mutability warning without generating bytecode - see https://github.com/ethereum/solidity/issues/2691
        return msg.data;
    }
}

abstract contract Ownable is Context {
    address private _owner;

    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);

    /**
     * @dev Initializes the contract setting the deployer as the initial owner.
     */
    constructor () {
        address msgSender = _msgSender();
        _owner = msgSender;
        emit OwnershipTransferred(address(0), msgSender);
    }

    /**
     * @dev Returns the address of the current owner.
     */
    function owner() public view virtual returns (address) {
        return _owner;
    }

    /**
     * @dev Throws if called by any account other than the owner.
     */
    modifier onlyOwner() {
        require(owner() == _msgSender(), "Ownable: caller is not the owner");
        _;
    }

    /**
     * @dev Leaves the contract without owner. It will not be possible to call
     * `onlyOwner` functions anymore. Can only be called by the current owner.
     *
     * NOTE: Renouncing ownership will leave the contract without an owner,
     * thereby removing any functionality that is only available to the owner.
     */
    function renounceOwnership() public virtual onlyOwner {
        emit OwnershipTransferred(_owner, address(0));
        _owner = address(0);
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     * Can only be called by the current owner.
     */
    function transferOwnership(address newOwner) public virtual onlyOwner {
        require(newOwner != address(0), "Ownable: new owner is the zero address");
        emit OwnershipTransferred(_owner, newOwner);
        _owner = newOwner;
    }
}

contract Pool is Ownable {
    
    mapping(bytes32 => uint256) fixtures; // fixtureid to betDetails
    bytes32[] fixturesInPool;

    constructor(){ 
       
    }
    
    function addFixturesToPool(bytes32 _homeTeamName, bytes32 _awayTeamName, bytes32 _matchPlayDate, uint _oddsHomeTeam, uint _oddsDraw, uint _oddsAwayTeam) public onlyOwner {
        bytes32 fixtureId = _createFixtureId(_homeTeamName, _awayTeamName, _matchPlayDate);
        fixtures[fixtureId] =  _setOdds(_oddsHomeTeam, _oddsDraw, _oddsAwayTeam);
    }

    function getFixturesInPool() external view returns(uint, uint, uint){
        
    }
    
    function _createFixtureId(bytes32 _homeTeamName, bytes32 _awayTeamName, bytes32 _matchPlayDate) private pure returns (bytes32){
        return keccak256(abi.encodePacked(_homeTeamName, _awayTeamName, _matchPlayDate));
    }
    
    function _setOdds(uint _oddsHomeTeam, uint _oddsDraw, uint _oddsAwayTeam) private pure returns (uint){
        require(_oddsHomeTeam < type(uint32).max, 'HT odds > max');
        require(_oddsDraw     < type(uint32).max, 'DW odds > max');
        require(_oddsAwayTeam < type(uint32).max, 'AT odds > max');
        uint fixtureDetails = (_oddsHomeTeam << 72) | (_oddsDraw << 40) | (_oddsAwayTeam << 8);
        return fixtureDetails;
    }
    
    function setMatchStatus(bytes32 _fixtureId, uint _matchStatus) external {
        require(_matchStatus < 3, 'invalid');
        uint _fixtureDetails = fixtures[_fixtureId];
        _fixtureDetails = _fixtureDetails & (0 << 4);
        _fixtureDetails = _fixtureDetails | (_matchStatus << 4);
        fixtures[_fixtureId] = _fixtureDetails;
        // emit event with gameid and new status
    }
    
    function setResult(bytes32 _fixtureId, uint _result) external onlyOwner {
        require(getMatchStatus(_fixtureId) == 2, 'denied');
        require(_result > 0 && _result < 4, 'invalid');

        uint _fixtureDetails = fixtures[_fixtureId];
        fixtures[_fixtureId] = _fixtureDetails | _result;
        // emit event with gameid and result
    }
    
    function getOdds(bytes32 _fixtureId) external view returns(uint, uint, uint){
        uint _fixtureDetails = fixtures[_fixtureId];
        uint _oddsHomeTeam = (_fixtureDetails >> 72) & type(uint32).max;
        uint _oddsDraw = (_fixtureDetails >> 40) & type(uint32).max;
        uint _oddsAwayTeam = (_fixtureDetails >> 8) & type(uint32).max;
        return (_oddsHomeTeam, _oddsDraw, _oddsAwayTeam);
    }
    
    function getMatchStatus(bytes32 _fixtureId) public view returns(uint){
        uint _fixtureDetails = fixtures[_fixtureId];
        uint matchStatus = (_fixtureDetails >> 4) & 15;
        return matchStatus;
    }
    
    function getResult(bytes32 _fixtureId) external view returns(uint){
        uint _fixtureDetails = fixtures[_fixtureId];
        uint result = _fixtureDetails & 15;
        return result;
    }

    
}

// 32 bits for homeTeamOdds = 32; remaining => 224; - 72
// 32 bits for drawOdds     = 64; remaining => 192; - 40
// 32 bits for awayTeamOdds = 96; remaining => 160; - 8
// 4 bits for matchStatus = 100; remaining => 156; - 4
// 4 bits for results = 104; remaining => 92; - 0

    //0x00geT0TheM00000000000000000000000000000N
    //0x0000000000000000000000000000000000000001
 //0xD0geT0TheM00000000000000000000000000000N



