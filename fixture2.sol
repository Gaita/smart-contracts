// SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.8.4;

contract Fixture {
    
    uint public electionDetails;
    
    function test() public pure returns(uint){
        return 2;
    }
    
    function test2() public pure returns(uint){
        require(test() == 2,"What are you doing?");
        return 100;
    }
    
    function setElectionStatus(uint _status) public {
        require(_status >= 0 && _status < 5, "Invalid status input");
        // electionDetails = (startBlock << 48) | (endBlock << 16) | (status << 8) | (numberOfcandidates);
        uint _electionDetails = electionDetails & (type(uint256).max ^ (type(uint8).max << 8));
        electionDetails = _electionDetails | (_status << 8);
    }
    
        
    function setToZero() public {
        // electionDetails = (startBlock << 48) | (endBlock << 16) | (status << 8) | (numberOfcandidates);
        electionDetails = (electionDetails & (type(uint256).max ^ (uint(type(uint8).max) << 8)));
    }
    
    
    function getElectionStatus() public view returns(uint){
        return (electionDetails >> 8) & type(uint8).max;
    }
}
