// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.7.0 <0.9.0;

contract CompetitionFactory {
    
    event CompetitionCreated(
        address indexed creator,
        address competition,
        bytes32 competitionName,
        uint date,
        uint indexed firstFixtureStartDateTime,
        uint indexed lastFixtureStartDateTime,
        uint
    );

    Competition[] public competitions;

    function createCompetition(bytes32 _competitionName, uint _firstFixtureStartDateTime, uint _lastFixtureStartDateTime, address[] memory _fixturesContractAddresses) external {
        // add check that fixtures are more than 0
        // add check that fixtures are less than 20
        // add check that _competitionPlayDateTime is in the future
        Competition competition = new Competition(_competitionName, msg.sender, _fixturesContractAddresses, packCompetitionStartDateTimes(_firstFixtureStartDateTime,_lastFixtureStartDateTime));
        competitions.push(competition);
        emit CompetitionCreated(msg.sender, address(competition), _competitionName, block.timestamp, _firstFixtureStartDateTime, _lastFixtureStartDateTime, competitions.length);
    }
    
    function packCompetitionStartDateTimes(uint _firstFixtureStartDateTime, uint _lastFixtureStartDateTime) public pure returns(uint){
        // electionDetails = (startBlock << 48) | (endBlock << 16) | (status << 8) | (numberOfcandidates);
        uint competitionStartDateTimes = 0 | (_firstFixtureStartDateTime << 32) | _lastFixtureStartDateTime;
        return competitionStartDateTimes;
    }
    
}

interface IFixture{
    function getFixtureStatus() external view returns(uint);
}


contract Competition {
    
    bytes32 public competitionName;
    address public creator;
    address public factoryAddress;
    uint public competitionPlayDateTimes;// and status

    mapping (address => uint) addressToBet;
    address[] public addressesThatHavePlacedBet;

    address[] public fixturesContractAddresses;
    
    constructor(bytes32 _competitionName, address _creator, address[] memory _fixturesContractAddresses, uint _competitionPlayDateTimes){
        competitionName = _competitionName;
        creator = _creator;
        fixturesContractAddresses = _fixturesContractAddresses;
        factoryAddress = msg.sender;
        competitionPlayDateTimes = _competitionPlayDateTimes;
    }
    
    event BetPlaced(address indexed bettor, uint betDetails, uint date);
    function placeABet(uint _betDetails) external {
        // bet will be a bitwise amd single bets: choice and fixture index
        addressToBet[msg.sender] = _betDetails;
        addressesThatHavePlacedBet.push(msg.sender);
        emit BetPlaced(msg.sender, _betDetails,  block.timestamp);
    }
    
    function getNumberOfAddressesThatHavePlacedBet() public view returns(uint){
        return addressesThatHavePlacedBet.length;
    }
    
    function getBettorCurrentPoints(address _bettor) public view returns(int) {
        int _points;
        for(uint i=0; i<fixturesContractAddresses.length; i++){
            require(getFixtureStatus(fixturesContractAddresses[i]) == 3, "fixtures not played and done");
            uint _choice = getChoiceForGivenFixtureIndex(i, _bettor);
            uint _oddsForChoice = getOddsOfAChoice(fixturesContractAddresses[i], _choice);
            uint _outcome = getFixtureOutcome(fixturesContractAddresses[i]);
            int _pointsToAdd;
            if(_choice == _outcome){
                _pointsToAdd = int256(_oddsForChoice);
            } else {
                _pointsToAdd = int256(_oddsForChoice)*-1;
            }
            _points += _pointsToAdd;
        }
        return _points;
    }
    
    function getChoiceForGivenFixtureIndex(uint _index, address _bettor) public pure returns(uint){
        
    }
    
    function getOddsOfAChoice(address _fixture, uint _choice) public pure returns(uint){
        
    }
    
    function getFixtureOutcome(address _fixture) public pure returns(uint){
        
    }

    // function getWinner(){
    //     // for loop, get odds and results for every fixture
    //     for(i=0; i<fixturesContractAddresses; i++){
    //         fixturesContractAddresses[i]
    //     }
    // }

    function getFixtureStatus(address _fixtureContractAddresses) view public returns(uint) {
        IFixture _fixture = IFixture(_fixtureContractAddresses);
        return _fixture.getFixtureStatus();
    }
    
    function getCompetitionStatus(address _fixtureContractAddresses) view public returns(uint) {
        IFixture _fixture = IFixture(_fixtureContractAddresses);
        return _fixture.getFixtureStatus();
    }
    
    
    // function placeABet(){}

}

// From right to left
// 4 bits for 1st  choice = 4;  remaining => 252;  - 0
// 4 bits for 2nd  choice = 8;  remaining => 248;  - 4
// 4 bits for 3rd  choice = 12; remaining => 244;  - 8
// 4 bits for 4th  choice = 16; remaining => 240;  - 12
// 4 bits for 5th  choice = 20; remaining => 236;  - 16
// 4 bits for 6th  choice = 24; remaining => 232;  - 20
// 4 bits for 7th  choice = 28; remaining => 228;  - 24
// 4 bits for 8th  choice = 32; remaining => 224;  - 28
// 4 bits for 9th  choice = 36; remaining => 220;  - 32
// 4 bits for 10th choice = 40; remaining => 216;  - 36
// 4 bits for 11th choice = 44; remaining => 212;  - 40
// 4 bits for 12th choice = 48; remaining => 208;  - 44
// 4 bits for 13th choice = 52; remaining => 204;  - 48
// 4 bits for 14th choice = 56; remaining => 200;  - 52
// 4 bits for 15th choice = 60; remaining => 196;  - 56
// 4 bits for 16th choice = 64; remaining => 192;  - 60
// 4 bits for 17th choice = 68; remaining => 188;  - 64
// 4 bits for 18th choice = 72; remaining => 184;  - 68
// 4 bits for 19th choice = 76; remaining => 180;  - 72
// 4 bits for 20th choice = 80; remaining => 176;  - 76



// 32 bits for startDateTime = 32;  remaining => 224; - 148
// 16 bits for homeTeamIndex = 48;  remaining => 208; - 132
// 32 bits for homeTeamOdds  = 80;  remaining => 176; - 100
// 32 bits for drawOdds      = 112; remaining => 144; - 68
// 16 bits for awayTeamIndex = 128; remaining => 128; - 52
// 32 bits for awayTeamOdds  = 160; remaining => 96;  - 20
// 4 bits  for matchStatus   = 164; remaining => 92;  - 16
// 8 bits  for homeTeamScore = 172; remaining => 84;  - 8
// 8 bits  for awayTeamScore = 180; remaining => 76;  - 0
 


