// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.7.0 <0.9.0;

contract Fixture {
    
    // bytes32 fixtureid;
    // hash of hometeamIndex, awayteamIndex, matchplaydate
    uint fixtureDetails;
    // stores odds, matchStatus, result, homeTeamNameIndex, awayTeamNameIndex, approved
    uint fixturePlayDate;

    address creator; // creator of this fixture smart contract;
    
    constructor(uint _oddsHomeTeam, uint _oddsDraw, uint _oddsAwayTeam){
        setOdds(_oddsHomeTeam, _oddsDraw, _oddsAwayTeam);
    }
    
    function convertToBytes32(bytes32 _homeTeamName, bytes32 _awayTeamName, bytes32 _matchPlayDate) public returns (bytes32){
        
    }
    
    function createFixtureId(bytes32 _homeTeamName, bytes32 _awayTeamName, bytes32 _matchPlayDate) public pure returns (bytes32){
        return keccak256(abi.encodePacked(_homeTeamName, _awayTeamName, _matchPlayDate));
    }
    
    function setOdds(uint _oddsHomeTeam, uint _oddsDraw, uint _oddsAwayTeam) private {
        require(_oddsHomeTeam < type(uint32).max, 'HT odds > max');
        require(_oddsDraw     < type(uint32).max, 'DW odds > max');
        require(_oddsAwayTeam < type(uint32).max, 'AT odds > max');
        fixtureDetails = (_oddsHomeTeam << 72) | (_oddsDraw << 40) | (_oddsAwayTeam << 8);
    }
    
    function setMatchStatus(uint _matchStatus) external {
        require(_matchStatus < 3, 'invalid');
        fixtureDetails = fixtureDetails & (0 << 4);
        fixtureDetails = fixtureDetails | (_matchStatus << 4);
        // emit event with gameid and new status
    }
    
    function setResult(uint _result) external{
        require(getFixtureStatus() == 2, 'denied');
        require(_result > 0 && _result < 4, 'invalid');
        fixtureDetails = fixtureDetails | _result;
        // emit event with gameid and result
    }
    
    function getOdds() external view returns(uint, uint, uint){
        uint _oddsHomeTeam = (fixtureDetails >> 72) & type(uint32).max;
        uint _oddsDraw = (fixtureDetails >> 40) & type(uint32).max;
        uint _oddsAwayTeam = (fixtureDetails >> 8) & type(uint32).max;
        return (_oddsHomeTeam, _oddsDraw, _oddsAwayTeam);
    }
    
    function getFixtureStatus() public view returns(uint){
        uint fixtureStatus = (fixtureDetails >> 4) & 15;
        return fixtureStatus;
    }
    
    function getResult() external view returns(uint){
        uint result = fixtureDetails & 15;
        return result;
    }

    function approveFixture() external view /**onlyOwner*/{
        // uint result = fixtureDetails & 15;
    }
    
    function disapproveFixture() external view /**onlyOwner*/{
        // uint result = fixtureDetails & 15;
    }
}
// 32 bits for startDateTime = 32; remaining => 224; - 72
// 32 bits for homeTeamOdds  = 64; remaining => 224; - 72
// 32 bits for drawOdds      = 96; remaining => 192; - 40
// 32 bits for awayTeamOdds  = 128; remaining => 160; - 8
// 4 bits for matchStatus    = 132; remaining => 156; - 4
// 4 bits for results        = 136; remaining => 92; - 0
 
// & - AND
// | - OR
// ~ - NOT
// << - Left shift
// >> - Right Shift
// ^ - XOR


