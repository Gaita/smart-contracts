/**
 *Submitted for verification at Etherscan.io on 2020-03-04
*/
// SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.8.4;
pragma experimental ABIEncoderV2;

abstract contract Context {
    function _msgSender() internal view virtual returns (address) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes calldata) {
        this; // silence state mutability warning without generating bytecode - see https://github.com/ethereum/solidity/issues/2691
        return msg.data;
    }
}

abstract contract Ownable is Context {
    address private _owner;

    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);

    /**
     * @dev Initializes the contract setting the deployer as the initial owner.
     */
    constructor () {
        address msgSender = _msgSender();
        _owner = msgSender;
        emit OwnershipTransferred(address(0), msgSender);
    }

    /**
     * @dev Returns the address of the current owner.
     */
    function owner() public view virtual returns (address) {
        return _owner;
    }

    /**
     * @dev Throws if called by any account other than the owner.
     */
    modifier onlyOwner() {
        require(owner() == _msgSender(), "Ownable: caller is not the owner");
        _;
    }

    /**
     * @dev Leaves the contract without owner. It will not be possible to call
     * `onlyOwner` functions anymore. Can only be called by the current owner.
     *
     * NOTE: Renouncing ownership will leave the contract without an owner,
     * thereby removing any functionality that is only available to the owner.
     */
    function renounceOwnership() public virtual onlyOwner {
        emit OwnershipTransferred(_owner, address(0));
        _owner = address(0);
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     * Can only be called by the current owner.
     */
    function transferOwnership(address newOwner) public virtual onlyOwner {
        require(newOwner != address(0), "Ownable: new owner is the zero address");
        emit OwnershipTransferred(_owner, newOwner);
        _owner = newOwner;
    }
}

contract GovernorAlpha is Ownable{
    using SafeMath for uint256;

    uint public electionDetails;
    // electionDetails = (startBlock << 56) | (startBlock << 48) | (endBlock << 16) | (status << 8) | (numberOfcandidates);
    // status 0 = notStarted 1= Active, 2=completed 3 = canceled , 4 = executed

    /// @notice candidateIndex to candidateId
    mapping (uint => bytes32) public candidates;
        
    /// @notice candidateId to votes
    mapping (bytes32 => uint) public votesPerCandidate;
    
    /// @notice voter address to their vote details
    mapping (address => uint) public votesCast; // use bitwise to store the vote details
    // voteDetails = (votes(128bytes) << 16) | (choice << 8) | (voted);
   
    // the choice will either be their twitterId or a bytes32 hash of their twitterid
    /// @notice An event emitted when a vote has been cast on a proposal
    event VoteCast(address voter, uint candidateIndex, uint votes);

    /// @notice An event emitted when a election has been canceled
    event ElectionCanceled(address electionContractAddress);
    
    // @notice 
    event electionResultsExecuted(bytes32 electionWinner);
    
    /// @notice The address of the Compound governance token
    CompInterface public comp;
    
    // pass the candidates. candidates can still be added later
    constructor(address tokenAddress, uint _delayInBlocks, uint _durationInBlocks){
        comp = CompInterface(tokenAddress);
        setElectionStartBlock(_delayInBlocks);
        setElectionEndBlock(_delayInBlocks, _durationInBlocks);
    }
    
    function setElectionStartBlock(uint _delayInBlocks) internal {
        uint _electionDetails = electionDetails & (type(uint256).max ^ (uint(type(uint32).max) << 48));
        electionDetails = _electionDetails | (block.number.add(_delayInBlocks)  << 48);
    }
    
    function setElectionEndBlock(uint _delayInBlocks, uint _durationInBlocks) internal {
        uint _electionDetails = electionDetails & (type(uint256).max ^ (uint(type(uint32).max) << 16));
        electionDetails = _electionDetails | (block.number.add(_delayInBlocks.add(_durationInBlocks))  << 16);
    }
    
    function setElectionStatus(uint _status) public {
        require(_status >= 0 && _status < 5, "Invalid status input");
        // electionDetails = (startBlock << 48) | (endBlock << 16) | (status << 8) | (numberOfcandidates);
        uint _electionDetails = electionDetails & (type(uint256).max ^ (uint(type(uint8).max) << 8));
        electionDetails = _electionDetails | (_status << 8);
    }
    
    function setElectionStatusToActive() public {
        // electionDetails = (startBlock << 48) | (endBlock << 16) | (status << 8) | (numberOfcandidates);
        require(block.number >= getElectionStartBlock(), "Election not yet started");
        require(block.number < getElectionEndBlock(), "Election ended");
        require(getElectionStatus() != 1, "status already set to active");
        require(getElectionStatus() != 3, "election canceled");
        uint _electionDetails = electionDetails & (type(uint256).max ^ (uint(type(uint8).max) << 8));
        electionDetails = _electionDetails | (1 << 8);
    }
    
    function addCandidate(bytes32 _candidateId) external onlyOwner {
        candidates[getNumberOfCandidates()] = _candidateId;
        addNumberOfCandidates();
    }

    function cancel() external onlyOwner {
        require(getElectionStatus() != 2, "Cannot cancel completed election");
        require(getElectionStatus() != 3, "Election already canceled");
        require(getElectionStatus() != 4, "Election completed and executed");
        setElectionStatus(3);

        emit ElectionCanceled(address(this));
    }

    function castVote(uint candidateIndex) public {
        return _castVote(msg.sender, candidateIndex);
    }

    function _castVote(address _voter, uint _candidateIndex) internal {
        require(getElectionStatus() == 1, "Cannot vote. Vote not started, completed or canceled");
        require(hasVoterVoted(_voter) == 0, "voter already voted");
        require(_candidateIndex < getNumberOfCandidates(), 'candidateIndex >= number of candidates');

        // put this functionality in the token.
        uint votes = comp.getPriorVotes(_voter, getElectionStartBlock());


        votesCast[msg.sender] = (votes << 16) | (_candidateIndex << 8) | 1;
        votesPerCandidate[candidates[_candidateIndex]] ++;
        emit VoteCast(_voter, _candidateIndex, votes);
    }
    
    function hasVoterVoted(address voter) public view returns(uint){
        return votesCast[voter] & type(uint8).max;
    }
    
    function getElectionStartBlock() public view returns(uint){
        return (electionDetails >> 48) & type(uint32).max;
    }
    
    function getElectionEndBlock() public view returns(uint){
        return (electionDetails >> 16) & type(uint32).max;
    }
    
    function getElectionStatus() public view returns(uint){
        return (electionDetails >> 8) & type(uint8).max;
    }
    
    function getElectionWinner() public view returns(bytes32){
        require(getElectionStatus() == 4, "Cannot get: election not started, ongoing, canceled or already executed");
        return candidates[((electionDetails >> 80) & type(uint8).max)];
    }
    
    function getNumberOfCandidates() public view returns(uint){
        return electionDetails & type(uint8).max;
    }
    
    function _setWinner(uint _candidateIndex) private {
        uint _electionDetails = electionDetails & (type(uint256).max ^ (uint(type(uint8).max) << 80));
        electionDetails = _electionDetails | (_candidateIndex << 80);
    } 
    
    function execute() public {
        require(getElectionStatus() == 2, "Cannot execute: election not started, ongoing, canceled or already executed");
        
        uint numberOfCandidates = getNumberOfCandidates();
        uint largest = 0; 
        uint i;
        uint _candidateIndex;

        for(i = 0; i < numberOfCandidates; i++){
            if(votesPerCandidate[candidates[i]]  > largest){
                largest = votesPerCandidate[candidates[i]];
                _candidateIndex = i;
            }
        }
        _setWinner(_candidateIndex);
        setElectionStatus(4);
        emit electionResultsExecuted(getElectionWinner());
    }
    
    function addNumberOfCandidates() internal {
        uint _electionDetails = electionDetails & (type(uint256).max ^ uint(type(uint8).max));
        electionDetails = _electionDetails | getNumberOfCandidates() + 1;
    }
}

interface CompInterface {
    function getPriorVotes(address account, uint blockNumber) external view returns (uint96);
}

library SafeMath {
    /**
     * @dev Returns the addition of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity's `+` operator.
     *
     * Requirements:
     *
     * - Addition cannot overflow.
     */
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c >= a, "SafeMath: addition overflow");

        return c;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity's `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        return sub(a, b, "SafeMath: subtraction overflow");
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting with custom message on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity's `-` operator.
     *
     * Requirements:
     *
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b <= a, errorMessage);
        uint256 c = a - b;

        return c;
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity's `*` operator.
     *
     * Requirements:
     *
     * - Multiplication cannot overflow.
     */
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        // Gas optimization: this is cheaper than requiring 'a' not being zero, but the
        // benefit is lost if 'b' is also tested.
        // See: https://github.com/OpenZeppelin/openzeppelin-contracts/pull/522
        if (a == 0) {
            return 0;
        }

        uint256 c = a * b;
        require(c / a == b, "SafeMath: multiplication overflow");

        return c;
    }

    /**
     * @dev Returns the integer division of two unsigned integers. Reverts on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity's `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        return div(a, b, "SafeMath: division by zero");
    }

    /**
     * @dev Returns the integer division of two unsigned integers. Reverts with custom message on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity's `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b > 0, errorMessage);
        uint256 c = a / b;
        // assert(a == b * c + a % b); // There is no case in which this doesn't hold

        return c;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * Reverts when dividing by zero.
     *
     * Counterpart to Solidity's `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        return mod(a, b, "SafeMath: modulo by zero");
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * Reverts with custom message when dividing by zero.
     *
     * Counterpart to Solidity's `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     *
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b != 0, errorMessage);
        return a % b;
    }
}
