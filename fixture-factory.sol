// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.7.0 <0.9.0;

abstract contract Context {
    function _msgSender() internal view virtual returns (address) {
        return msg.sender;
    }

    function _msgData() internal view virtual returns (bytes calldata) {
        return msg.data;
    }
}

abstract contract Ownable is Context {
    address private _owner;

    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);

    constructor() {
        _setOwner(_msgSender());
    }

    function owner() public view virtual returns (address) {
        return _owner;
    }

    modifier onlyOwner() {
        require(owner() == _msgSender(), "Ownable: caller is not the owner");
        _;
    }

    function renounceOwnership() public virtual onlyOwner {
        _setOwner(address(0));
    }

    function transferOwnership(address newOwner) public virtual onlyOwner {
        require(newOwner != address(0), "Ownable: new owner is the zero address");
        _setOwner(newOwner);
    }

    function _setOwner(address newOwner) private {
        address oldOwner = _owner;
        _owner = newOwner;
        emit OwnershipTransferred(oldOwner, newOwner);
    }
}

contract FixtureFactory is Ownable {
    
    event FixtureCreated(
        address indexed creator,
        address fixture,
        uint fixtureDetails,
        uint indexed date,
        uint indexed fixturePlayDateTime,
        uint
    );

    Fixture[] public fixtures;
    //put three mappings of fixtures

    mapping(uint => bytes32) public indexToTeamName;
    mapping(bytes32 => uint) public teamNameToIndex;
    
    uint public numberOfTeamNames;
    bytes32 public _bytes32;
    
    function createFixture(uint _fixtureDetails) external {
        // add check if playDateTime is in the future
        // add check if odds are above 100, if not throw error
        // add check if scores are zero.
        require(indexToTeamName[getHomeTeamIndex(_fixtureDetails)] != 0, "Home team name does not exists");
        require(indexToTeamName[getAwayTeamIndex(_fixtureDetails)] != 0, "Away team name does not exists");
        Fixture fixture = new Fixture(_fixtureDetails, msg.sender, address(this));
        fixtures.push(fixture);
        emit FixtureCreated(msg.sender, address(fixture), _fixtureDetails, block.timestamp, getFIxtureStartDateTime(_fixtureDetails), fixtures.length);
    }
    
    event TeamNameAdded( uint date, address creator, bytes32 teamNameToCreate);
    function addTeamName(bytes32 _teamNameToCreate) external {
        require(teamNameToIndex[_teamNameToCreate] == 0 , "Team name already exists");
        numberOfTeamNames ++;
        indexToTeamName[numberOfTeamNames] = _teamNameToCreate;
        teamNameToIndex[_teamNameToCreate] = numberOfTeamNames;
        emit TeamNameAdded(block.timestamp, msg.sender, _teamNameToCreate);
    }
    
    function changeTeamName(bytes32 _oldTeamName, bytes32 _newTeamName) external {
        // for 'deletion set teamName value to zero'
        require(teamNameToIndex[_oldTeamName] != 0 , "Error: old team name does not exists");
        require(teamNameToIndex[_newTeamName] == 0 , "Error: new team already name exists");
        teamNameToIndex[_newTeamName] = teamNameToIndex[_oldTeamName];
        indexToTeamName[teamNameToIndex[_oldTeamName]] = _newTeamName;
    }
    
    function getTeamNameIndex(bytes32 _teamName) view external returns(uint){
        return teamNameToIndex[_teamName];
    }
    
    function getHomeTeamIndex(uint _fixtureDetails) pure private returns(uint) {
        return (_fixtureDetails >> 132) & type(uint16).max;
    }
    
    function getAwayTeamIndex(uint _fixtureDetails) pure private returns(uint) {
        return (_fixtureDetails >> 52) & type(uint16).max;
    }
    
    function getFIxtureStartDateTime(uint _fixtureDetails) pure private returns(uint) {
        return (_fixtureDetails >> 148) & type(uint32).max;
    }
    
    function getNumberOfCreatedFixtures() view external returns(uint) {
        return fixtures.length;
    }
    
}

contract Fixture {
    
    uint public fixtureDetails;
    address public creator;
    address public factoryAddress;
    
    constructor(uint _fixtureDetails, address _creator, address _factoryAddress){
        fixtureDetails = _fixtureDetails;
        creator = _creator;
        factoryAddress = _factoryAddress;
    }
    
    // function setElectionStatus(uint _status) public {
    //     require(_status >= 0 && _status < 5, "Invalid status input");
    //     electionDetails = (startBlock << 48) | (endBlock << 16) | (status << 8) | (numberOfcandidates);
    //     uint _electionDetails = electionDetails & (type(uint256).max ^ (type(uint8).max << 8));
    //     electionDetails = _electionDetails | (_status << 8);
    // }
    
    function setResult(uint _result) external /**onlyOwner*/{
        // require that 2hrs have passed since game started.
        require(getFIxtureStartDateTime() + 2 hours < block.timestamp, "Error: match has not ended");
        require(getMatchStatus() == 2, 'denied');
        require(_result > 0 && _result < 4, 'invalid');
        fixtureDetails = fixtureDetails | _result;
        // emit event with gameid and result
    }
    
    function getFIxtureTimeStatus() public view returns(uint){
        // return the status of the match based on 
        uint _fIxtureStartDateTime = getFIxtureStartDateTime();
        if(_fIxtureStartDateTime > block.timestamp){ return 0; }
        if(_fIxtureStartDateTime < block.timestamp && _fIxtureStartDateTime + 2 hours > block.timestamp){ return 1; }
        if(_fIxtureStartDateTime + 2 hours < block.timestamp){ return 2; } else { return 0; }
    }
    
    function getFIxtureStartDateTime() public view returns(uint){
        uint matchStatus = (fixtureDetails >> 104 & uint(type(uint32).max));
        return matchStatus;
    }
    
    function getMatchStatus() public view returns(uint){
        uint matchStatus = (fixtureDetails >> 4) & 15;
        return matchStatus;
    }
    
    function getResult() external view returns(uint){
        uint result = fixtureDetails & 15;
        return result;
    }

    function approveFixture() external view /**onlyOwner*/{
        // uint result = fixtureDetails & 15;
    }
    
    function disapproveFixture() external view /**onlyOwner*/{
        // uint result = fixtureDetails & 15;
    }
    
}

// 32 bits for startDateTime = 32;  remaining => 224; - 148
// 16 bits for homeTeamIndex = 48;  remaining => 208; - 132
// 32 bits for homeTeamOdds  = 80;  remaining => 176; - 100
// 32 bits for drawOdds      = 112; remaining => 144; - 68
// 16 bits for awayTeamIndex = 128; remaining => 128; - 52
// 32 bits for awayTeamOdds  = 160; remaining => 96;  - 20
// 4 bits  for matchStatus   = 164; remaining => 92;  - 16
// 8 bits  for homeTeamScore = 172; remaining => 84;  - 8
// 8 bits  for awayTeamScore = 180; remaining => 76;  - 0
 
/**
 * matchStatus: 0 =  notStarted, 1 = beingPlayed, 2 = PlayedAndDone, 3 = cancelled.  
 * result: 0 =  notStarted, 1 = beingPlayed, 2 = PlayedAndDone, 3 = cancelled.  
*/
 




