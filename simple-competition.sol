// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.7.0 <0.9.0;

contract CompetitionFactory {
    
    address public admin;
    
    event CompetitionCreated(
        address indexed creator,
        address competition,
        uint createdAt,
        uint numberOfCompetitionsCreated
    );
    
    mapping(address => address) competitionAddressToCreator;
    uint numberOfCompetitions;

    constructor(){
        admin = msg.sender;
    }

    function createCompetition(uint _betAmountInWei) external {
        Competition competition = new Competition(msg.sender, admin, _betAmountInWei);
        competitionAddressToCreator[address(competition)] = msg.sender;
        numberOfCompetitions += 1;
        emit CompetitionCreated(msg.sender, address(competition), block.timestamp, numberOfCompetitions);
    }
}

contract Competition {
    
    address public admin;
    address public creator;
    address public factoryAddress;

    mapping(address => bool) public hasAddressPlacedABet;
    uint numberOfAddressesThatHavePlacedBet;
    
    uint public betAmountInWei;
    
    address public winner;
    uint public totalFinalAmountInCompetition;
    bool public winningsWithdrawn;
    bool public creatorsRewardWithdrawn;

    constructor(address _creator, address _admin, uint _betAmountInWei){
        creator = _creator;
        admin = _admin;
        factoryAddress = msg.sender;
        betAmountInWei = _betAmountInWei;
    }
    
    event BetPlaced(address indexed bettor, uint date);
    function placeABet() external payable {
        require(msg.value == betAmountInWei, "amount not exact");
        require(!hasAddressPlacedABet[msg.sender], "an address can only bet once");
        hasAddressPlacedABet[msg.sender] = true;
        numberOfAddressesThatHavePlacedBet ++;
        emit BetPlaced(msg.sender,  block.timestamp);
    }
    
    function getNumberOfAddressesThatHavePlacedBet() public view returns(uint){
        return numberOfAddressesThatHavePlacedBet;
    }
    
    function getAmoutToBeWon() public view returns(uint){
        return totalFinalAmountInCompetition * uint(7)/uint(10);
    }

    function setWinner(address _winner) public {
        require(msg.sender == admin, "forbidden");
        winner = _winner;
        totalFinalAmountInCompetition = address(this).balance;
    }

    function withdrawWinnings() public {
        require(msg.sender == winner, "only winner can withdraw winnings");
        require(!winningsWithdrawn, "winnings already withdrawn");
        winningsWithdrawn = true;
        uint winnings = totalFinalAmountInCompetition * uint(7)/uint(10);
        (bool success, ) = msg.sender.call{value:winnings,  gas: 5000}("");
        require(success, "Withdraw failed.");
    }
    
    function withdrawCreatorsReward() public {
        require(msg.sender == creator, "only winner can withdraw winnings");
        require(winner != address(0), "competition not done");
        require(!creatorsRewardWithdrawn, "winnings already withdrawn");
        creatorsRewardWithdrawn = true;
        uint creatorsReward = totalFinalAmountInCompetition * uint(1)/uint(10);
        (bool success, ) = msg.sender.call{value:creatorsReward,  gas: 5000}("");
        require(success, "Withdraw failed.");
    }
    
    function withdraw() public {
        require(msg.sender == admin, "only admin");
        (bool success, ) = msg.sender.call{value:address(this).balance,  gas: 5000}("");
        require(success, "Withdraw failed.");
    }
    
}

