// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.7.0 <0.9.0;

contract MyFactory {
    
    uint betAmountInWei = 1000000000000000;
    bool b;
    
    // function placeABet(uint bb) payable public {
    //     require(bb == 1000000000000000, "whhy?");
    //         (bool success,) = msg.sender.call{value: bb}("");
    //         b = success;
    // }
    function receie() external payable {
        require(msg.value == 1 ether, "Incorrect amount"); // transaction always fails, even if I send exactly 1ETH.
    }
    
    function getDivided(uint numerator, uint denominator) pure public returns(uint quotient, uint remainder) {
        quotient  = numerator / denominator;
        remainder = numerator - denominator * quotient;
    }
    
    function divide(uint256 num1, uint256 num2) public pure returns(uint256){
        (uint l, ) =  getDivided(num1, num2);
        return l * 7;
    }
    
    function dive()pure public returns(uint){
        return uint(100) * uint(7)/uint(10);
    }
    
}



 